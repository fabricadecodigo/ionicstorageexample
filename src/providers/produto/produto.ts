import { Injectable } from '@angular/core';
import { Storage } from '@ionic/storage';
import { DatePipe } from '@angular/common';

@Injectable()
export class ProdutoProvider {

  constructor(private storage: Storage, private datepipe: DatePipe) { }

  public insert(produto: any) {
    // gerando um id para o produto com base na data e hora
    let key = this.datepipe.transform(new Date(), "ddMMyyyyHHmmss");
    return this.save(key, produto);
  }

  public update(key: string, produto: any) {
    return this.save(key, produto);
  }

  private save(key: string, produto: any) {
    return this.storage.set(key, produto);
  }

  public getAll() {
    return new Promise((resolve, reject) => {
      let produtos = [];

      this.storage.forEach((value: any, key: string, iterationNumber: Number) => {
        let produto: any = {}
        produto.key = key;
        produto.dados = value;
        produtos.push(produto);
      })
        .then(() => {
          return resolve(produtos);
        })
        .catch((error) => {
          return reject(error);
        });
    });
  }

  public remove(key: string) {
    return this.storage.remove(key);
  }

  public getCategoriaDescricao(valor: string) {
    if (valor = '1') {
      return 'Hambúrguer';
    } else if (valor = '2') {
      return 'Refrigerante';
    } else if (valor = '3') {
      return 'Batata';
    }
  }

  public getTamanhoDescricao(valor: string) {
    if (valor = 'p') {
      return 'Pequeno';
    } else if (valor = 'm') {
      return 'Médio';
    } else if (valor = 'g') {
      return 'Grande';
    }
  }
}
