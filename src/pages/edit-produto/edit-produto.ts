import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, ToastController } from 'ionic-angular';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ProdutoProvider } from '../../providers/produto/produto';

@IonicPage()
@Component({
  selector: 'page-edit-produto',
  templateUrl: 'edit-produto.html',
})
export class EditProdutoPage {
  form: FormGroup;
  key: string;

  constructor(
    public navCtrl: NavController, public navParams: NavParams, private toast: ToastController,
    private formBuilder: FormBuilder, private produtoProvider: ProdutoProvider) {

      this.key = navParams.data.key;

      // dados padrão do cadastro
      let produto = {
        categoria: '1',
        tamanho: 'p',
        ativo: true
      }

      if (navParams.data.produto) {
        produto = navParams.data.produto;
      }

      this.createForm(produto);
  }

  private createForm(produto: any) {
    this.form = this.formBuilder.group({
      nome: [produto.nome, [Validators.required, Validators.minLength(2)]],
      descricao: [produto.descricao, [Validators.required, Validators.minLength(2)]],
      categoria: [produto.categoria],
      preco: [produto.preco, [Validators.required]],
      dataCadastro: [produto.dataCadastro, [Validators.required]],
      tamanho: [produto.tamanho],
      ativo: [produto.ativo]
    })
  }

  salvar() {
    if (this.form.valid) {

      this.salvarProduto(this.form.value)
        .then(() => {
          this.toast.create({ message: 'Produto salvo com sucesso.', duration: 2000 }).present();
          this.navCtrl.pop();
        })
        .catch(() => {
          this.toast.create({ message: 'Erro ao salvar o produto.', duration: 2000 }).present();
        });
    }
  }

  private salvarProduto(produto: any) {
    if (this.key) {
      return this.produtoProvider.update(this.key, produto);
    } else {
      return this.produtoProvider.insert(produto);
    }
  }

}
